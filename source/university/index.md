---
layout: markdown_page
title: University
---

## What is GitLab University

Version control will be the standard for creation of any type of media in the
future. Today it's still pretty hard to master.

GitLab University has as a goal to teach Git, GitLab and everything that relates
to that to anyone willing to listen and participate.

## Process

### Suggesting improvements

If you would like to teach ca class or participate or help in any way please submit a merge request and assign it to [Job](https://gitlab.com/u/JobV).

If you have suggestions for additional courses you would like to see,
please submit a merge request to add an upcoming class, assign to [Chad](https://gitlab.com/u/chadmalchow) and /cc [Job](https://gitlab.com/u/JobV).

### Adding classes

1. All training materials of any kind should be added to [the university repo](https://gitlab.com/gitlab-org/University) to ensure they are available to a broad audidence (don't use any other repo or storage for training materials).
1. Please link any new materials from the /university page
1. Don't make materials that are needlessly specific to one group of people, try to keep the wording broad and inclusive (don't make things for only GitLab Inc. people, only interns, only customers, etc.).
1. To allow people to contribute to them all content should be in git.
1. The content can go in a subdirectory under /university
1. Please link your materials from this page, either under 'Non sales bootcamp classes' or 'Other'
1. To make, view or modify the slides of the classes use [Deckset](http://www.decksetapp.com/) or [RevealJS](http://lab.hakim.se/reveal-js/), do not use Powerpoint or Google Slides since this prevents everyone from contributing.
1. Please upload any video recordings to our Youtube channel, we prefer them to be public, if needed they can be unlisted but if so they should be linked from this page.
1. Please create a merge request and assign to [Job](https://gitlab.com/u/JobV).

### Recordings

There is a [YouTube playlist with all recordings](https://www.youtube.com/playlist?list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e).

### Webcasts

For GitLab Inc. team members only there is a university class every Thursday at 5PM UTC, send Job a message to sign up.

### Sales bootcamp week 1 to 3

To prepare new salespeople at GitLab, an intense 30 day program has been completed.  Each week there are learning goals and content to support the learning of the salesperson.  The goal of bootcamp is to have every salesperson prepared to help buyers navigate the purchase of GitLab EE.

### Week 1 - Learning goal is to be able create a Group, Project, Issue and merge request.

1. Understanding [terminology](https://about.gitlab.com/university/glossary/).  Please create a merge request for any term that does not have an answer or to add a term that you feel is relevant but is not on this page.

1. Understandging Version Control Systems
    - [Presentation](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.g72f2e4906_2_29)

1. Operating Systems and How Git Works
    - [Operating Systems and How Git Works](https://drive.google.com/a/gitlab.com/file/d/0B41DBToSSIG_OVYxVFJDOGI3Vzg/view?usp=sharing) recorded date: 2015-10-01

1. Introduction to Git
    - [Intro to Git](https://www.codeschool.com/account/courses/try-git)
    - Supporting: [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html)

1. Create a GitLab Account
    - [Create a GitLab Account](https://courses.platzi.com/classes/git-gitlab/concepto/first-steps/create-an-account-on-gitlab/material/)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

1. Add SHH key to GitLab
    - [Create and Add your SHH key to GitLab](https://www.youtube.com/watch?v=54mxyLo3Mqk)

1. Repositories, Projects and Groups
    - [Recording](https://www.youtube.com/watch?v=4TWfh1aKHHw&index=1&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e) recorded date: 2015-12-10

1. Creating a Project in GitLab
    - [Recording](https://www.youtube.com/watch?v=7p0hrpNaJ14)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

1. Issues and Merge Requests
    - [Recording](https://www.youtube.com/watch?v=raXvuwet78M)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

### Week 2 - Learning goal is to communicate the key differentiators between GitLab offerings and GitLab competition

1. Ecosystem
    - [Recording 2015-11-05](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
    - [GitLab Ecosystem slides](https://docs.google.com/presentation/d/1vCU-NbZWz8NTNK8Vu3y4zGMAHb5DpC8PE5mHtw1PWfI/edit)

1. Understanding DevOps?
    - [Understanding DevOps](https://youtu.be/HpZBnc07q9o)
    - [DevOps at IBM](https://www.youtube.com/user/IBMRational)
    - [DevOps Where To Start](https://www.youtube.com/watch?v=CSrKwP1QrjE)
    - [Agile & DevOps](https://www.youtube.com/watch?v=WqoVeGFjK9k)
    - [Problem Solving with DevOps](https://www.youtube.com/watch?v=pTq9hFBWPeM)

1. [Compare GitLab versions](https://about.gitlab.com/features/#compare)

1. [GitLab compared to other tools](https://about.gitlab.com/comparison/)

1. [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)

1. JIRA and Jenkins integrations in GitLab
    - [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415). Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video

1. [GitLab Workshop Part 1: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-1/material/)

1. [GitLab Workshop Part 2: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-23370/material/)

1. [GitLab Workshop Part 3: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-3/material/)

1. [GitLab Workshop Part 4: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-4/material/)

1. GitLab Flow
    - [Recording of what it is](https://www.youtube.com/watch?v=UGotqAUACZA)
    - [GitLab Flow blog post](https://about.gitlab.com/2014/09/29/gitlab-flow/)
    - [GitLab Flow documentation](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)

1. GitLab Integrations

    - Supporting: [Documentation on Integrating Jira with GitLab](http://doc.gitlab.com/ee/integration/jira.html) and [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).  Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video
    - Supporting: [Atlassian Crowd feature request](http://feedback.gitlab.com/forums/176466-general/suggestions/4324384-integration-with-crowd)
    - Supporting: [Documentation on Integrating Jenkins with GitLab](http://doc.gitlab.com/ee/integration/jenkins.html)
    - Supporting: [Documentation on Integrating Bamboo with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/project_services/bamboo.md)
    - Supporting: [Documentation on Integrating Slack with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/integration/slack.md)

1. Migrating to GitLab from SVN/GitHub Enterprise/BitBucket/Perforce
    - Supporting: [Migrating from BitBucket/Stash](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_bitbucket.html)
    - Supporting: [Migrating from GitHub](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_github.html)
    - Supporting: [Migrating from SVN](http://doc.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
    - Supporting: [Migrating from Fogbugs](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_fogbugz.html)

### Week 3 - Learning goal is to be able to pitch GitLab Enterprise Edition focused on the key selling points of EE.

1. [Compare GitLab versions](https://about.gitlab.com/features/#compare)

1. Scalability and High Availability
    - [Recording 2015-12-03](https://www.youtube.com/watch?v=cXRMJJb6sp4&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=2)
    - [High Availability Documentation](https://about.gitlab.com/high-availability/)

1. Managing LDAP, Active Directory
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=HPMjM-14qa8)

1. Managing Permissions within EE
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=DjUoIrkiNuM)

1. GitLab 8.2
    - [Recording 2015-11-19](https://www.youtube.com/watch?v=09RLHyMFfpA&index=3&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
    - [Slides](https://gitlab.com/gitlab-org/University/blob/master/classes/8.2.markdown) 2015-11-19

1. Upcoming in EE and Big files in Git (Git LFS, Annex)
    - [Upcoming in EE](https://gitlab.com/gitlab-org/University/blob/master/classes/upcoming_in_ee.md)
    - [Big files in Git (Git LFS, Annex)](https://gitlab.com/gitlab-org/University/blob/master/classes/git_lfs_and_annex.md)

1. [GitLab Workshop Part 1: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-1/material/)

1. [GitLab Workshop Part 2: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-23370/material/)

1. [GitLab Workshop Part 3: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-3/material/)

1. [GitLab Workshop Part 4: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-4/material/)

1. [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)

1. [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

1. [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=ae7b72c61347030e8aa75328ed4b8660)

### Week 4 - Goal is to create a lead, check if lead is using CE, create a task, convert lead to opportunty, add a product to the opportunity, create a quote, close win the opportunity, create a renewal opportunity and invoice the client.

1. Review in order, learning material on our [Sales Procss](https://about.gitlab.com/handbook/sales-process/) page.

1. Login to [Salesforce.com](http://www.salesforce.com/), you should receive an email asking you to change your password.  Once you are in Salesforce, please familiarize yourself wth these reports/views as they will be critical in helping you manage your business.
    - [Your Current Month Pipeline](https://na34.salesforce.com/00O61000001uYbM) This view is to focus you on what you are committing to closing this month.  Are you in the right stage? What is needed to advance the sale to the next stage?
    - [Your Total Pipeline](https://na34.salesforce.com/00O61000001uYbR) This view should be used to identify where you are going and where you need focus to ensure you are successful.  What needs to to close and/or where you need to build up your pipleine - new business, expansion, add-on
    - [Your Leads](https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1) This view should be used to make sure you are followign up on each lead in a timely manner and have a plan on how you qualify or disqualify a lead.
        * [Your Personal Dashboard](https://na34.salesforce.com/01Z61000000J0gx) This dashboard should be used to understand where you have been, where you are at, where are you going and do you have the pipeline to get to where you need to be.
        * [Accounts you Own](https://na34.salesforce.com/001?fcf=00B61000001XPLz) This view is to be used to identify expansion opportunities, who you have neglected and the mix of customers to prospects you are working

### Non sales bootcamp classes

#### GitLab Wiki

- Upcoming

#### GitLab Activity Log

- Upcoming

#### Branching and Forking

- Upcoming

#### GitLab Omnibus

- [Recording of what it is](https://www.youtube.com/watch?v=XTmpKudd-Oo)
- [Recording of how to install](https://www.youtube.com/watch?v=Q69YaOjqNhg)
- Supporting: [Configuring an external PostgreSQL database](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#using-a-non-packaged-postgresql-database-management-server)
- Supporting: [Configuring an external MySQL database](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#using-a-mysql-database-management-server-enterprise-edition-only)

#### Continuous Integration and Runners

- Upcoming
- [Continuous Delivery vs Continuous Deployment](https://www.youtube.com/watch?v=igwFj8PPSnw)
- [GitLab CI](https://about.gitlab.com/gitlab-ci/)
- Supporting: [Documentation on Integrating Jenkins with GitLab](http://doc.gitlab.com/ee/integration/jenkins.html)
- Supporting: [Documentation on Integrating Bamboo with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/project_services/bamboo.md)

#### GitLab Mirroring

- Upcoming

#### GitLab Pages

- Upcoming
- Supporting: [Documentation on How to set up and use Pages](http://doc.gitlab.com/ee/pages/README.html)

#### GitLab Markdown

- Upcoming
- Supporting: [Markdown](http://doc.gitlab.com/ce/markdown/markdown.html)

#### Other

* [GitLab documentation](http://doc.gitlab.com/)
* [GitLab architecture for noobs](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/doc/development/architecture.md)
* [Innersourcing article](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)
* [Platzi training course](https://courses.platzi.com/courses/git-gitlab/)
* [Sales Onboarding materials](https://about.gitlab.com/handbook/sales-onboarding/)
* [GitLab Direction](https://about.gitlab.com/direction/)
* [Amazon's transition to Continuous Delivery](https://www.youtube.com/watch?v=esEFaY0FDKc)
* [3rd party tool comparison](http://technologyconversations.com/2015/10/16/github-vs-gitlabs-vs-bitbucket-server-formerly-stash/)
* [State of Dev Ops 2015 Report by Puppet Labs](https://puppetlabs.com/sites/default/files/2015-state-of-devops-report.pdf) Insightful Chapters to understand the Impact of Continuous Delivery on Performance (Chapter 4), the Application Architecture (Chapter 5) and How IT Managers can help their teams win (Chapter 6).
* [Customer review of GitLab with talking points on why they prefer GitLab](https://www.enovate.co.uk/web-design-blog/2015/11/25/gitlab-review/)
* [2011 WSJ article by Mark Andreeson - Software is Eating the World](http://www.wsj.com/articles/SB10001424053111903480904576512250915629460)
* [2014 Blog post by Chris Dixon - Software eats software development](http://cdixon.org/2014/04/13/software-eats-software-development/)
* [2015 Venture Beat article - Actually, Open Source is Eating the World](http://venturebeat.com/2015/12/06/its-actually-open-source-software-thats-eating-the-world/)
* [Customer Use-Cases](https://about.gitlab.com/handbook/use-cases/)
* [Why Git and GitLab slide deck](https://docs.google.com/a/gitlab.com/presentation/d/1RcZhFmn5VPvoFu6UMxhMOy7lAsToeBZRjLRn0LIdaNc/edit?usp=drive_web)
* [Git Workshop](https://docs.google.com/presentation/d/1JzTYD8ij9slejV2-TO-NzjCvlvj6mVn9BORePXNJoMI/edit?usp=drive_web)
* [Client Assessment of GitLab versus GitHub](https://docs.google.com/a/gitlab.com/spreadsheets/d/18cRF9Y5I6I7Z_ab6qhBEW55YpEMyU4PitZYjomVHM-M/edit?usp=sharing) INTERNAL ACCESS ONLY

## Training Material

### User Training

Git workshop for GitLab end-users. This training covers everything users need
to configure their environment and understand the GitLab interface.

- [Slides](https://gitlab.com/gitlab-org/University/blob/master/training/user_training.md)
can be viewed using [Deckset](http://www.decksetapp.com/).
